#!/bin/bash
# This script needs to be executed from the germinate artifacts directory

set -e

GERMINATE_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
CUR_DIR=`pwd`
TARGET_BUILD_DEPENDS_DEVELOPMENT=$GERMINATE_DIR/target-build-depends-development.txt

SCAN_APERTIS=0
SCAN_DEBIAN=0
FILTER=""
FILTER_ONLY_RUNTIME=0

DEBIAN_DISTRIBUTION=debian
DEBIAN_SEED=apertis
DEBIAN_MIRROR=http://deb.debian.org/debian/
DEBIAN_RELEASE=bookworm
DEBIAN_COMPONENTS=main,non-free,non-free-firmware

APERTIS_DISTRIBUTION=apertis
APERTIS_SEED=apertis
APERTIS_MIRROR=http://repositories.apertis.org/apertis
APERTIS_RELEASE=v2026dev1
APERTIS_COMPONENTS=target,development,sdk,non-free
APERTIS_COMPONENTS_SPACE=`echo $APERTIS_COMPONENTS | tr ',' ' '`

filter_seed(){
  FILTER=$1
  SEED_DIR=$2

  FILTER_STR="#.*$FILTER"

  for c in $APERTIS_COMPONENTS_SPACE
  do
    f=$SEED_DIR/$c
    grep "$FILTER_STR" $f | sponge $f || true
  done
}

germinate_dist(){
    DISTRIBUTION=$1
    SEED=$2
    MIRROR=$3
    RELEASE=$4
    COMPONENTS=$5

    cd $CUR_DIR
    mkdir -p $DISTRIBUTION
    cd $DISTRIBUTION

    if [ -n "$FILTER" ]
    then
      SEED_TMP=${SEED}_tmp
      rm -r ${GERMINATE_DIR}/${SEED_TMP}
      cp -r ${GERMINATE_DIR}/${SEED} ${GERMINATE_DIR}/${SEED_TMP}
      SEED=$SEED_TMP
      filter_seed $FILTER $GERMINATE_DIR/$SEED
    fi

    germinate -S $GERMINATE_DIR -s $SEED -m $MIRROR -d $RELEASE -a amd64 -c $COMPONENTS > log.txt

    #Remove intermediate output files
    rm -r rdepends
}

germinate_all(){
  if [ "$SCAN_DEBIAN" = "1" ]
  then
      germinate_dist $DEBIAN_DISTRIBUTION $DEBIAN_SEED $DEBIAN_MIRROR $DEBIAN_RELEASE $DEBIAN_COMPONENTS
  fi

  if [ "$SCAN_APERTIS" = "1" ]
  then
      germinate_dist $APERTIS_DISTRIBUTION $APERTIS_SEED $APERTIS_MIRROR $APERTIS_RELEASE $APERTIS_COMPONENTS
  fi
}

generate_dependencies() {
  # Generate list for $1 component

  cat /dev/null > all-sources-new.txt

  # Extract dependencies
  for comp in $APERTIS_COMPONENTS_SPACE
  do
    if [ "$SCAN_DEBIAN" = "1" ]
    then
      awk 'NR>2 { print $1 }' "../debian/$comp.sources" > "$comp-run-dependencies.txt"
      awk 'NR>2 { print $1 }' "../debian/$comp.build-sources" > "$comp-build-dependencies.txt"
    fi
    if [ "$SCAN_APERTIS" = "1" ]
    then
      awk 'NR>2 { print $1 }' "../apertis/$comp.sources" >> "$comp-run-dependencies.txt"
      awk 'NR>2 { print $1 }' "../apertis/$comp.build-sources" >> "$comp-build-dependencies.txt"
    fi
    sort -u -o $comp-run-dependencies.txt{,}  
    sort -u -o $comp-build-dependencies.txt{,}

    cat $comp-run-dependencies.txt $comp-build-dependencies.txt >> all-sources-new.txt
  done

  if [ "$SCAN_DEBIAN" = "1" ]
  then
    grep "Unknown" ../debian/log.txt | grep -v "debhelper-compat" > unknown.txt
  fi
  if [ "$SCAN_APERTIS" = "1" ]
  then
    grep "Unknown" ../apertis/log.txt | grep -v "debhelper-compat" >> unknown.txt
  fi

  sort -u -o all-sources-new.txt{,}  
}

export_sources() {
  cat /dev/null > all-sources-current.txt
  for comp in $APERTIS_COMPONENTS_SPACE
  do
    osc ls "apertis:${APERTIS_RELEASE}:$comp" | sort -u > "$comp-sources-current.txt"
    cat $comp-sources-current.txt >> all-sources-current.txt
  done

  sort -u -o all-sources-current.txt{,}
}

generate_target() {
  # Since rust and go packages are statically linked
  # the build dependencies include code that will be included in the binary
  # for this reason include them in the list of packages
  # However, this filter will also include packages that are tools
  # for this reason a filter is applied to avoid the known ones
  grep -E "^rust-|^golang-" target-build-dependencies.txt > target-static-linked-sources.txt
  if [ -f "$TARGET_BUILD_DEPENDS_DEVELOPMENT" ]
  then
    sort $TARGET_BUILD_DEPENDS_DEVELOPMENT > target-build-depends-development.txt
    comm -23 target-static-linked-sources.txt target-build-depends-development.txt | sponge target-static-linked-sources.txt
  fi

  cat target-run-dependencies.txt > target-sources-new.txt
  if [ $FILTER_ONLY_RUNTIME != "1" ]
  then
    cat target-static-linked-sources.txt >> target-sources-new.txt
  fi

  sort -u -o target-sources-new.txt{,}
}

generate_development(){
  cat /dev/null > development-sources-new.txt
  if [ $FILTER_ONLY_RUNTIME != "1" ]
  then
    cat target-build-dependencies.txt >> development-sources-new.txt
  fi
  cat development-run-dependencies.txt >> development-sources-new.txt
  if [ $FILTER_ONLY_RUNTIME != "1" ]
  then
    cat development-build-dependencies.txt >> development-sources-new.txt
    cat non-free-build-dependencies.txt >> development-sources-new.txt
  fi

  sort -u -o development-sources-new.txt{,}

  comm -23 development-sources-new.txt target-sources-new.txt | sponge development-sources-new.txt
}

generate_sdk() {
  cat /dev/null > sdk-sources-new.txt
  cat sdk-run-dependencies.txt >> sdk-sources-new.txt
  if [ $FILTER_ONLY_RUNTIME != "1" ]
  then
    cat sdk-build-dependencies.txt >> sdk-sources-new.txt
  fi

  sort -u -o sdk-sources-new.txt{,}

  comm -23 sdk-sources-new.txt target-sources-new.txt | sponge sdk-sources-new.txt
  comm -23 sdk-sources-new.txt development-sources-new.txt | sponge sdk-sources-new.txt

}

generate_non-free() {
  cat non-free-run-dependencies.txt >> non-free-sources-new.txt

  sort -u -o non-free-sources-new.txt{,}
}

generate_report() {
  #-1                      suppress column 1 (lines unique to FILE1)
  #-2                      suppress column 2 (lines unique to FILE2)
  #-3                      suppress column 3 (lines that appear in both files)

  for comp in $APERTIS_COMPONENTS_SPACE
  do
    # Packages to keep intact
    comm -12 $comp-sources-new.txt $comp-sources-current.txt > $comp-keep.txt
    # Packages missing
    comm -23 $comp-sources-new.txt $comp-sources-current.txt > $comp-missing.txt
    # Packages no longer needed in component
    comm -23 $comp-sources-current.txt $comp-sources-new.txt  > $comp-drop-move.txt
    # Packages that need to be moved
    comm -23 $comp-missing.txt all-sources-current.txt > $comp-import.txt
    # Packages no longer needed at all
    comm -23 $comp-drop-move.txt all-sources-new.txt > $comp-drop.txt
  done

  # Packages that need to be moved from development to target
  comm -12 target-missing.txt development-sources-current.txt > target-development-move.txt
  # Packages that need to be moved from target to development
  comm -12 development-missing.txt target-sources-current.txt > development-target-move.txt
  # Packages that need to be moved from development to sdk
  comm -12 sdk-missing.txt development-sources-current.txt > sdk-development-move.txt
}

while getopts 'adf:r:s:t' opt; do
    case "$opt" in
        a)
            SCAN_APERTIS=1
            ;;
        d)
            SCAN_DEBIAN=1
            ;;
        f)
            FILTER=${OPTARG}
            ;;
        r)
            APERTIS_RELEASE=${OPTARG}
            ;;
        s)
            DEBIAN_RELEASE=${OPTARG}
            ;;
        t)
            FILTER_ONLY_RUNTIME=1
            ;;
    esac
done
shift "$(($OPTIND -1))"


VARS="SCAN_APERTIS SCAN_DEBIAN APERTIS_RELEASE DEBIAN_RELEASE FILTER FILTER_ONLY_RUNTIME"
for var in $VARS
do
  echo "$var is set to ${!var}"
done

if [ "$SCAN_APERTIS" != "1" ] && [ "$SCAN_DEBIAN" != "1" ]
then
  SCAN_APERTIS=1
fi

germinate_all

cd $CUR_DIR
mkdir -p report
cd report
rm -rf *

# Generate dependencies for development, target, sdk component
generate_dependencies

export_sources

generate_target
generate_development
generate_sdk
generate_non-free

generate_report
