Germinate
=========
Germinate takes a list of seed packages (apertis source packages) and a mirror of
bookworm distribution, and expands the dependencies for these sets of seed packages.

Below are the steps to create the seed packages from apertis packages and the output.
Seed files are present in the `apertis` folder. These folder contains one file per component:
target, development, sdk and non-free and a STRUCTURE file which shows the interaction
between them.

Seed packages are added to the seed file in below format, lines not starting with " *" are
ignored
```
 * [Binary package]
 * %[Source package] # entries beginning with "%" expand to all the binaries packages
 * ![Binary package] # entries beginning with "!" are blacklisted
 * !%[Source package] # entries beginning with "!%" expand a blacklist to all the binaries packages
 ```

The output of `germinate` provides several files, however, the relevant ones are:
`*.build-sources`: source packages for component seed's build-dependencies
`*.sources`: source packages for component seed's runtime-dependencies

With these files the script `run-germinate.sh` is used to generate a report to help maintainers to
plan a package import/rebase. This script
- Extract the package list from OBS to have the current state of the distribution
- Generates the new list of packages based on `germinate` output
- Computes a set of outputs to report packages to import, drop or move

This repo can be used in different scenarios where a big list of packages will be changed

Rebase
======
During a rebase all the packages are rebased on top of a new Debian release. This is a very
time consuming task, so having a good plan is important. In this context, the dependencies of
seed packages might change, so this repo will help to populate the list of packages to keep,
to import, to move and to drop.

The information about the new dependencies are obtained from Debian, however, since Apertis
carries some deltas, this information is not 100 % accurate. For this reason the workflow should be:

- Configure variables APERTIS_RELEASE pointing to the current release and DEBIAN_RELEASE pointing to the new release
- Configure variables SCAN_APERTIS=0 and SCAN_DEBIAN=1
- Germinate
- Take the list of packages to keep and use them to trigger the rebase on top of Debian.
- Take the list of Apertis specific packages and process them
- Check OBS missing dependencies and fix them

With this process, only the relevant packages will be rebased, dropping non relevant ones in
the new Apertis release.

Massive import
==============
In some rare cases, new packages with a large set of dependencies need to be imported. Under
this context, having the list of packages to import in each component is very useful,
to ensure this task can be accomplished and to avoid problems like:
- Packages with no friendly license in target
- Massive import of useless packages (packages we don't really care)

To accomplish this the workflow is:
- Configure variables APERTIS_RELEASE and DEBIAN_RELEASE pointing to the current releases
- Configure variables SCAN_APERTIS=1 and SCAN_DEBIAN=1
- Germinate
- Update the seed list with the new entries
- Configure variables SCAN_APERTIS=1 and SCAN_DEBIAN=1
- Germinate
- Compare the difference between last and the previous run

Repo check
==========
Besides the rebase, this repo should be used to check the repo status. This ensures
that:
- Seeds are up to date
- Packages meant to be in each component are in the right one
- There are no missing runtime dependencies
- There are no missing build dependencies (OBS should have complain)

The workflow in this case is:
- Configure variables APERTIS_RELEASE and DEBIAN_RELEASE pointing to the current releases
- Configure variables SCAN_APERTIS=1 and SCAN_DEBIAN=0
- Germinate
- Check the report to confirm the health, specially the file unknown.txt which highlights unknown packages

Blacklist update
================
This is not a real use case, however, it is useful to keep the blacklist up to date,
as this information allow maintainers to understand the delta against Debian. Also,
by doing this, the scanning against Debian will provide more accurate results
in general.

For this case the workflow is:
- Configure variables APERTIS_RELEASE and DEBIAN_RELEASE pointing to the current releases
- Configure variables SCAN_APERTIS=0 and SCAN_DEBIAN=1
- Germinate
- Review the outcome to check the packages that germinate reports as missing and update
the blacklist when it is needed

Filtering
=========
In some scenarios it is useful to play with a subset of packages, for instance
during the rebase might be useful to focus in packages like the ones used to
bootstrap Apertis. To support this feature, this repo uses comment lines to
add useful information which can be used to filtering by setting variables

FILTER: Allows to filter the packages based on comments, like bootstrap, apertis
or request

FILTER_ONLY_RUNTIME: Allows to focus in runtime dependencies

For instance, to focus on the packages needed for bootstrap without caring about
build dependencies use FILTER=bootstrap and FILTER_ONLY_RUNTIME=1

References
==========
- https://wiki.debian.org/Germinate
- https://manpages.debian.org/stable/germinate/germinate.1.en.html
